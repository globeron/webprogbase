const bodyParser = require('body-parser');

const busboy = require('busboy-body-parser');

var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var usersRouter = require('./routes/users');
var gamesRouter = require('./routes/games');
var mediaRouter = require('./routes/media');

const express = require('express');
const app = express();

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);

//body-parser setup
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//busboy-body-parser setup
const optionsBusboy = {
  limit: '5mb',
  multi: false,
};
app.use(busboy(optionsBusboy));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/users', usersRouter);
app.use('/api/games', gamesRouter);
app.use('/api/media', mediaRouter);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'lab2-swagger',
            title: 'lab2',
            version: '1.1.17',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);


//catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
