var express = require('express');

var router = express.Router();

var userController = require('../controllers/users');

/**
 * User router
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

 /**
 * Masive
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<User>} User - a page with users
 */


/* GET users listing. */
router
  .get('/', userController.getUsers)
  .get('/:id(\\d+)', userController.getUserById);

module.exports = router;
