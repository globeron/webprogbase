<<<<<<< HEAD
const fs = require('fs');

class JsonStorage
{
    constructor(filePath)
    {
        this.filePath = filePath;
    }
    
    readItems()
    {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    get nextId()
    {
        return this.readItems()['nextId'];
    }

    incrementNextId()
    {
        const jsonArray = this.readItems();
        jsonArray['nextId']++;
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json);
    }

    writeItems(items)
    {
        const jsonArray = this.readItems();
        jsonArray['items']=items['items'];
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json);
    }
};

=======
const fs = require('fs');

class JsonStorage
{
    constructor(filePath)
    {
        this.filePath = filePath;
    }
    
    readItems()
    {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    get nextId()
    {
        return this.readItems()['nextId'];
    }

    incrementNextId()
    {
        const jsonArray = this.readItems();
        jsonArray['nextId']++;
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json);
    }

    writeItems(items)
    {
        const jsonArray = this.readItems();
        jsonArray['items']=items['items'];
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json);
    }
};

>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
module.exports = JsonStorage;