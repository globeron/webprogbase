<<<<<<< HEAD
var gameRepository = require('../repositories/gameRepository');
const Game = require('./../models/game');
const gameStorage = new gameRepository('./data/games.json');

module.exports =
{
    getGames(req, res)
    {
        let jsonArray = gameStorage.getGames();
        let page = parseInt(req.query.page);
        let per_page = parseInt(req.query.per_page);
        let skipped_items = (page - 1) * per_page;
        let newJsonArray = [];
        var counter = 0 ;
        for(var i = 0 + skipped_items; i< per_page+skipped_items && i<jsonArray.length; i++)
        {
            newJsonArray[counter] = jsonArray[i];
            counter++;
        }
        res.json(newJsonArray);
    },
    getGameById(req, res)
    {
        const found = gameStorage.getGameById(req.params.id);
        if (found!=null)
        {
            
            res.json(found);
        }
        else
        {
            res.sendStatus(404);
        }
    },
    addGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        
        res.status(201).json(gameStorage.getGameById(gameStorage.addGame(new_item)));
    },
    updateGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        if(gameStorage.updateGame(new_item))
        {
            res.json(gameStorage.getGameById(req.body.id));
        }
        else
            res.sendStatus(404);
    },
    deleteGame(req, res)
    {
        const item_id = req.params.id;
        if(gameStorage.getGameById(item_id)!=null)
        {
            res.json(gameStorage.getGameById(item_id));
            gameStorage.delteGame(item_id);
        }
        else
            res.sendStatus(404);
    }
=======
var gameRepository = require('../repositories/gameRepository');
const Game = require('./../models/game');
const gameStorage = new gameRepository('./data/games.json');

module.exports =
{
    getGames(req, res)
    {
        let jsonArray = gameStorage.getGames();
        let page = parseInt(req.query.page);
        let per_page = parseInt(req.query.per_page);
        let skipped_items = (page - 1) * per_page;
        let newJsonArray = [];
        var counter = 0 ;
        for(var i = 0 + skipped_items; i< per_page+skipped_items && i<jsonArray.length; i++)
        {
            newJsonArray[counter] = jsonArray[i];
            counter++;
        }
        res.json(newJsonArray);
    },
    getGameById(req, res)
    {
        const found = gameStorage.getGameById(req.params.id);
        if (found!=null)
        {
            
            res.json(found);
        }
        else
        {
            res.sendStatus(404);
        }
    },
    addGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        
        res.status(201).json(gameStorage.getGameById(gameStorage.addGame(new_item)));
    },
    updateGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        if(gameStorage.updateGame(new_item))
        {
            res.json(gameStorage.getGameById(req.body.id));
        }
        else
            res.sendStatus(404);
    },
    deleteGame(req, res)
    {
        const item_id = req.params.id;
        if(gameStorage.getGameById(item_id)!=null)
        {
            res.json(gameStorage.getGameById(item_id));
            gameStorage.delteGame(item_id);
        }
        else
            res.sendStatus(404);
    }
>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
}