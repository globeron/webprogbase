<<<<<<< HEAD
var express = require('express');

var router = express.Router();

var gameController = require('../controllers/games');



/**
 * Game router
 * @route GET /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {User.model} 200 - Game object
 * @returns {Error} 404 - Game not found
 */

 /**
 * Get masive
 * @route GET /api/games
 * @group Games - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Game>} Games - a page with games
 */

 /**
 * Add game
 * @route POST /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 201 - added Game object
 */

 /**
 * Change game
 * @route PUT /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 200 - changed Game object
 */
 
/**
 * Del game
 * @route DELETE /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {Game.model} 200 - deleted Game object
 * @returns {Error} 404 - Game not found
 */


/* GET games listing. */
router
  .get('/', gameController.getGames)
  .get('/:id(\\d+)', gameController.getGameById)
  .post('/new', gameController.addGame)
  .get('/new', (req,res)=>res.render('game_new'))
  .put('/', gameController.updateGame)
  .post('/:id(\\d+)', gameController.deleteGame);

=======
var express = require('express');

var router = express.Router();

var gameController = require('../controllers/games');



/**
 * Game router
 * @route GET /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {User.model} 200 - Game object
 * @returns {Error} 404 - Game not found
 */

 /**
 * Get masive
 * @route GET /api/games
 * @group Games - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Game>} Games - a page with games
 */

 /**
 * Add game
 * @route POST /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 201 - added Game object
 */

 /**
 * Change game
 * @route PUT /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 200 - changed Game object
 */
 
/**
 * Del game
 * @route DELETE /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {Game.model} 200 - deleted Game object
 * @returns {Error} 404 - Game not found
 */


/* GET games listing. */
router
  .get('/', gameController.getGames)
  .get('/:id(\\d+)', gameController.getGameById)
  .post('/new', gameController.addGame)
  .get('/new', (req,res)=>res.render('game_new'))
  .put('/', gameController.updateGame)
  .post('/:id(\\d+)', gameController.deleteGame);

>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
module.exports = router;