<<<<<<< HEAD
var express = require('express');

var router = express.Router();

var mediaController = require('../controllers/media');

/**
 * Add media
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 200 - added image id
 */

 /**
 * Get media
 * @route GET /api/media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required - id of the Media - eg: 1
 * @returns {Media.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

 /* GET media listing. */
router
.post('/', mediaController.addMedia)
.get('/:id(\\d+)', mediaController.getMediaById);

module.exports = router;
=======
var express = require('express');

var router = express.Router();

var mediaController = require('../controllers/media');

/**
 * Add media
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 200 - added image id
 */

 /**
 * Get media
 * @route GET /api/media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required - id of the Media - eg: 1
 * @returns {Media.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

 /* GET media listing. */
router
.post('/', mediaController.addMedia)
.get('/:id(\\d+)', mediaController.getMediaById);

module.exports = router;
>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
