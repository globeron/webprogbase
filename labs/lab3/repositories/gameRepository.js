<<<<<<< HEAD
const Game = require('./../models/game');
const jsonStorage = require('./../jsonStorage');

class GameRepository extends jsonStorage
{
    getGames()
    { 
        const items = this.readItems();
        let games = [Game];
        let counter = 0;
        for (const item of items['items'])
        {
            games[counter] = new Game(
                item.id,
                item.name,
                item.studio,
                item.price,
                item.score,
                item.date,
                item.imageUrl
            );
            counter++;
        }
        return games;
    }

    getGameById(id) 
    {
        const items = this.readItems();
        for (const item of items['items'])
        {
            if (item.id == id)
            {
                return new Game(
                    item.id,
                    item.name,
                    item.studio,
                    item.price,
                    item.score,
                    item.date,
                    item.imageUrl
                );
            }
        }
        return null; 
    }

    addGame(item)
    {
        const items = this.readItems();
        const NewGame = new Game(
            items['nextId'],
            item.name,
            item.studio,
            item.price,
            item.score,
            item.date,
            item.imageUrl
        );
        this.incrementNextId();
        items['items'].push(NewGame);
        this.writeItems(items);
        return NewGame.id;
    }

    updateGame(NewGame)
    {
        let counter = 0;
        const items = this.readItems();
        for (let i = 0; i<items['items'].length; i++)
        {
            if (NewGame.id == items['items'][counter]['id'])
            {
                items['items'].splice(counter,1,NewGame);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }

    delteGame(id)
    {
        let counter = 0;
        const items = this.readItems();
        for (const item of items['items'])
        {
            
            if (item.id == id)
            {
                
                items['items'].splice(counter,1);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }
};

=======
const Game = require('./../models/game');
const jsonStorage = require('./../jsonStorage');

class GameRepository extends jsonStorage
{
    getGames()
    { 
        const items = this.readItems();
        let games = [Game];
        let counter = 0;
        for (const item of items['items'])
        {
            games[counter] = new Game(
                item.id,
                item.name,
                item.studio,
                item.price,
                item.score,
                item.date,
                item.imageUrl
            );
            counter++;
        }
        return games;
    }

    getGameById(id) 
    {
        const items = this.readItems();
        for (const item of items['items'])
        {
            if (item.id == id)
            {
                return new Game(
                    item.id,
                    item.name,
                    item.studio,
                    item.price,
                    item.score,
                    item.date,
                    item.imageUrl
                );
            }
        }
        return null; 
    }

    addGame(item)
    {
        const items = this.readItems();
        const NewGame = new Game(
            items['nextId'],
            item.name,
            item.studio,
            item.price,
            item.score,
            item.date,
            item.imageUrl
        );
        this.incrementNextId();
        items['items'].push(NewGame);
        this.writeItems(items);
        return NewGame.id;
    }

    updateGame(NewGame)
    {
        let counter = 0;
        const items = this.readItems();
        for (let i = 0; i<items['items'].length; i++)
        {
            if (NewGame.id == items['items'][counter]['id'])
            {
                items['items'].splice(counter,1,NewGame);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }

    delteGame(id)
    {
        let counter = 0;
        const items = this.readItems();
        for (const item of items['items'])
        {
            
            if (item.id == id)
            {
                
                items['items'].splice(counter,1);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }
};

>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
module.exports = GameRepository;