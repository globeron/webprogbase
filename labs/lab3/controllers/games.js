<<<<<<< HEAD
var gameRepository = require('../repositories/gameRepository');
const MediaRepository = require('../repositories/mediaRepository');
const Game = require('./../models/game');
const Media = require('./../models/media');
const gameStorage = new gameRepository('./data/games.json');
const mediaStorage = new MediaRepository('./data/media.json');


module.exports =
{
    getGames(req, res)
    {
        let jsonArray = gameStorage.getGames();
        let page = parseInt(req.query.page) || 1;
        const searchBy = req.query.search || "";
        let per_page = 4;
        let skipped_items = (page - 1) * per_page;
        //
        for(var i = 0; i<jsonArray.length; i++)
        {
            var str = jsonArray[i].name;
            if(!str.includes(searchBy))
            {
                jsonArray.splice(i, 1);
                i--;
            }
        }
        //
        let games = [];
        var counter = 0 ;
        for(var i = 0 + skipped_items; i< per_page+skipped_items && i<jsonArray.length; i++)
        {
            games[counter] = jsonArray[i];
            counter++;
        }
        let pages_total = Math.ceil(jsonArray.length / per_page);
        let pageNext = page + 1;
        let pagePrev = page - 1;
        let actvPrev;
        let actvNext;
        if(page<=1)
            actvPrev="disabled";
        if(page >= pages_total)
            actvNext="disabled";
        res.render('games',
        {
            games,
            page,
            pageNext,
            pagePrev,
            actvPrev,
            actvNext,
            searchBy
        })
    },
    getGameById(req, res)
    {
        const found = gameStorage.getGameById(req.params.id);
        if (found!=null)
        {
            
            res.render('game',
            {
                found
            });
        }
        else
        {
            res.sendStatus(404);
        }
    },
    addGame(req, res)
    {
        let new_media = new Media
        (
            req.body.id,
            req.files
        )
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date,
            mediaStorage.addMedia(new_media)
        )
        res.status(201);
        res.redirect("/games/"+gameStorage.getGameById(gameStorage.addGame(new_item)).id);
            
    },
    updateGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        if(gameStorage.updateGame(new_item))
        {
            res.json(gameStorage.getGameById(req.body.id));
        }
        else
            res.sendStatus(404);
    },
    deleteGame(req, res)
    {
        const item_id = req.params.id;
        if(gameStorage.delteGame(item_id))
        {
            res.redirect("/games");
        }
        else
            res.sendStatus(404);
    }
=======
var gameRepository = require('../repositories/gameRepository');
const MediaRepository = require('../repositories/mediaRepository');
const Game = require('./../models/game');
const Media = require('./../models/media');
const gameStorage = new gameRepository('./data/games.json');
const mediaStorage = new MediaRepository('./data/media.json');


module.exports =
{
    getGames(req, res)
    {
        let jsonArray = gameStorage.getGames();
        let page = parseInt(req.query.page) || 1;
        const searchBy = req.query.search || "";
        let per_page = 4;
        let skipped_items = (page - 1) * per_page;
        //
        for(var i = 0; i<jsonArray.length; i++)
        {
            var str = jsonArray[i].name;
            if(!str.includes(searchBy))
            {
                jsonArray.splice(i, 1);
                i--;
            }
        }
        //
        let games = [];
        var counter = 0 ;
        for(var i = 0 + skipped_items; i< per_page+skipped_items && i<jsonArray.length; i++)
        {
            games[counter] = jsonArray[i];
            counter++;
        }
        let pages_total = Math.ceil(jsonArray.length / per_page);
        let pageNext = page + 1;
        let pagePrev = page - 1;
        let actvPrev;
        let actvNext;
        if(page<=1)
            actvPrev="disabled";
        if(page >= pages_total)
            actvNext="disabled";
        res.render('games',
        {
            games,
            page,
            pageNext,
            pagePrev,
            actvPrev,
            actvNext,
            searchBy
        })
    },
    getGameById(req, res)
    {
        const found = gameStorage.getGameById(req.params.id);
        if (found!=null)
        {
            
            res.render('game',
            {
                found
            });
        }
        else
        {
            res.sendStatus(404);
        }
    },
    addGame(req, res)
    {
        let new_media = new Media
        (
            req.body.id,
            req.files
        )
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date,
            mediaStorage.addMedia(new_media)
        )
        res.status(201);
        res.redirect("/games/"+gameStorage.getGameById(gameStorage.addGame(new_item)).id);
        res.render('game',
            {
                found
            });
            
    },
    updateGame(req, res)
    {
        let new_item = new Game
        (
            req.body.id,
            req.body.name,
            req.body.studio,
            req.body.price,
            req.body.score,
            req.body.date
        )
        if(gameStorage.updateGame(new_item))
        {
            res.json(gameStorage.getGameById(req.body.id));
        }
        else
            res.sendStatus(404);
    },
    deleteGame(req, res)
    {
        const item_id = req.params.id;
        if(gameStorage.delteGame(item_id))
        {
            res.redirect("/games");
        }
        else
            res.sendStatus(404);
    }
>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
}