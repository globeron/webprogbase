const mongoose = require('mongoose');

const DeveloperSchema = new mongoose.Schema({
    name: { type: String, required: true },
    founded: { type: Date },
    headquarters: { type: String },
 });

 module.exports = DeveloperSchema;