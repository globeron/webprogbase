const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema({
    name: { type: String, required: true },
    price: { type: Number },
    developer: { type: mongoose.mongo.ObjectId, ref: "developers" },
    score: { type: Number, min: 0, max: 10 },
    date: { type: Date, default: Date.now },
    imageUrl: { type: String},
 });

 module.exports = GameSchema;