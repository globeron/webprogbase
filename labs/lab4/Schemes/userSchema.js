const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    login: { type: String, required: true },
    fullname: { type: String },
    role: { type: Number, default: 0 },
    registeredAt: { type: Date, default: Date.now },
    isEnable: { type: Boolean, default: true}
 });

 module.exports = UserSchema;