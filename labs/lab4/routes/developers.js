var express = require('express');

var router = express.Router();

var developerController = require('../controllers/developers');



/**
 * Developer router
 * @route GET /api/developers/{id}
 * @group developers - developer operations
 * @param {integer} id.path.required - id of the Developer - eg: 1
 * @returns {User.model} 200 - Developer object
 * @returns {Error} 404 - Developer not found
 */

 /**
 * Get masive
 * @route GET /api/developers
 * @group developers - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Developer>} developers - a page with developers
 */

 /**
 * Add developer
 * @route POST /api/developers
 * @group developers - developer operations
 * @param {Developer.model} id.body.required - new Developer object
 * @returns {Developer.model} 201 - added Developer object
 */

 /**
 * Change developer
 * @route PUT /api/developers
 * @group developers - developer operations
 * @param {Developer.model} id.body.required - new Developer object
 * @returns {Developer.model} 200 - changed Developer object
 */
 
/**
 * Del developer
 * @route DELETE /api/developers/{id}
 * @group developers - developer operations
 * @param {integer} id.path.required - id of the Developer - eg: 1
 * @returns {Developer.model} 200 - deleted Developer object
 * @returns {Error} 404 - Developer not found
 */


/* GET developers listing. */
router
  .get('/', developerController.getDevelopers)
  .get('/new', (req,res)=>res.render('developer_new'))
  .post('/new', developerController.addDeveloper)
  .get('/:id', developerController.getDeveloperById)
  .put('/:id', developerController.updateDeveloper)
  .post('/:id', developerController.deleteDeveloper);

module.exports = router;