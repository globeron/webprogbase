var express = require('express');

var router = express.Router();

var gameController = require('../controllers/games');



/**
 * Game router
 * @route GET /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {User.model} 200 - Game object
 * @returns {Error} 404 - Game not found
 */

 /**
 * Get masive
 * @route GET /api/games
 * @group Games - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Game>} Games - a page with games
 */

 /**
 * Add game
 * @route POST /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 201 - added Game object
 */

 /**
 * Change game
 * @route PUT /api/games
 * @group Games - game operations
 * @param {Game.model} id.body.required - new Game object
 * @returns {Game.model} 200 - changed Game object
 */
 
/**
 * Del game
 * @route DELETE /api/games/{id}
 * @group Games - game operations
 * @param {integer} id.path.required - id of the Game - eg: 1
 * @returns {Game.model} 200 - deleted Game object
 * @returns {Error} 404 - Game not found
 */


/* GET games listing. */
router
  .get('/', gameController.getGames)
  .get('/:id/change', gameController.renderChangeGame)
  .post('/:id/change', gameController.updateGame)
  .get('/new', gameController.renderAddGame)
  .post('/new', gameController.addGame)
  .get('/:id', gameController.getGameById)
  .post('/:id', gameController.deleteGame);

module.exports = router;