const User = require('../models/user.js');
const jsonStorage = require('../jsonStorage.js');
//
 
class UserRepository extends jsonStorage
{
    async getUsers()
    { 
        const items = this.readItems();
        let users = [User];
        let counter = 0;
        for (const item of items['items'])
        {
            users[counter] = new User(
                item.id,
                item.login,
                item.fullname,
                item.role,
                item.registeredAt,
                item.avaUrl,
                item.isEnable
            );
            counter++;
        }
        //
        return users;
    }

    async getUserById(id) 
    {
        const items = this.readItems();
        for (const item of items['items'])
        {
            if (item.id == id)
            {
                return new User(
                    item.id,
                    item.login,
                    item.fullname,
                    item.role,
                    item.registeredAt,
                    item.avaUrl,
                    item.isEnable
                );
            }
        }
        return null; 
    }
};

module.exports = UserRepository;