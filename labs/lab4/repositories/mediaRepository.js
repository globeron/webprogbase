const Media = require('./../models/media');
const jsonStorage = require('./../jsonStorage');
const fs = require('fs');

class MediaRepository extends jsonStorage
{
    getMediaById(id)
    { 
        const items = this.readItems();
        for (const item of items['items'])
        {
            if (item.id == id)
            {
                return new Media(
                    item.id,
                    item.file
                );
            }
        }
        return null; 
    }
    addMedia(item)
    { 
        const items = this.readItems();
        const NewMedia = new Media(
            items['nextId'],
            item.file
        );
        this.incrementNextId();
        items['items'].push(NewMedia);
        this.writeItems(items);
        return NewMedia.id;
    }
}

module.exports = MediaRepository;