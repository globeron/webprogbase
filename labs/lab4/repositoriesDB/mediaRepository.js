const fs = require('fs');

const config = require('../config');
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

class MediaRepository {
    getMediaById(id) {
        const items = this.readItems();
        for (const item of items['items']) {
            if (item.id == id) {
                return new Media(
                    item.id,
                    item.file
                );
            }
        }
        return null;
    }
    async addMedia(path)
    {
        return cloudinary.v2.uploader.upload(path);
    }
}


module.exports = MediaRepository;