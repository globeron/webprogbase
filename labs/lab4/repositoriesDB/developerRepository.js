var mongoose = require('mongoose');
const developerSchema = require('../Schemes/developerSchema.js');
const DeveloperModel = mongoose.model('developers', developerSchema);

class DeveloperRepositoryDB {
    async getDevelopers() {
        try {
            return (await DeveloperModel.find().sort({ name: 'asc' })).map(x => x.toJSON());
        }
        catch (err) {
            throw err;
        }
    }
    async getDevelopersByName(name)
    {
        try {
            return (await DeveloperModel.findOne({ name: name }));
        }
        catch (err) {
            throw err;
        }
    }
    async getDeveloperById(id) {
        try {
            const devDoc = await DeveloperModel.findById(id);
            if (devDoc == null)
                return null;
            return devDoc.toJSON();
        }
        catch (err) {
            return err;
        }
    }
    async addDeveloper(item) {
        try {
            const NewDev = new DeveloperModel({
                name: item.name,
                founded: item.founded,
                headquarters: item.headquarters
            });
            return await NewDev.save();
        }
        catch (err) {
            throw (err);
        }
    }

    async updateDeveloper(NewDev) {
        try {
            const devDoc = await DeveloperModel.findByIdAndUpdate(NewDev.id, new DeveloperModel({
                name: item.name,
                founded: item.founded,
                headquarters: item.headquarters
            }));
            if (devDoc == null)
                return false;
            return true;
        }
        catch (err) {
            throw (err);
        }
    }

    async delteDeveloper(id) {
        try {
            const devDoc = await DeveloperModel.findByIdAndDelete(id);
            if (devDoc == null)
                return false;
            return true;
        }
        catch (err) {
            throw (err);
        }
    }
    
};

module.exports = DeveloperRepositoryDB;