var mongoose = require('mongoose');
const UserSchema = require('../Schemes/userSchema.js');
const UserModel = mongoose.model('user', UserSchema);
 
class UserRepository
{
    async getUsers()
    { 
        try
        {
            return (await UserModel.find().sort({ registeredAt: -1 })).map(x => x.toJSON());
        }
        catch (err)
        {
            throw err;
        }
    }

    async getUserById(id) 
    {
        try
        {
            const userDoc = await UserModel.findById(id).exec();
            if(userDoc == null)
                return null;
            return userDoc.toJSON();
        }
        catch (err)
        {
            return err;
        }
    }
    async addUser(item) {
        try {
            
            const NewUser = new UserModel({
                login: item.login,
                fullname: item.fullname,
                role: item.role,
                registeredAt: item.registeredAt,
                isEnable: item.isEnable,
            });
            return await NewUser.save();
        }
        catch (err) {
            throw (err);
        }
    }
};

module.exports = UserRepository;