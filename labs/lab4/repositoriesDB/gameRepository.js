var mongoose = require('mongoose');
const gameSchema = require('../Schemes/gameSchema.js');
const GameModel = mongoose.model('games', gameSchema);

class GameRepositoryDB {
    async getGames() {
        try {
            return (await GameModel.find().sort({ name: 'asc' })).map(x => x.toJSON());
        }
        catch (err) {
            throw err;
        }
    }

    async getGameById(id) {
        try {
            const gameDoc = await GameModel.findById(id).populate("developer");
            if (gameDoc == null)
                return null;
            return gameDoc.toJSON();
        }
        catch (err) {
            return err;
        }
    }
    async addGame(item) {
        try {
            
            const NewGame = new GameModel({
                name: item.name,
                developer: item.developer,
                price: item.price,
                score: item.score,
                date: item.date,
                imageUrl: item.imageUrl,
            });
            return await NewGame.save();
        }
        catch (err) {
            throw (err);
        }
    }

    async updateGame(NewGame) {
        try {
            const gameDoc = await GameModel.findByIdAndUpdate(NewGame.id, {
                name: NewGame.name
            });
            if (gameDoc == null)
                return false;
            return true;
        }
        catch (err) {
            throw (err);
        }
    }

    async delteGame(id) {
        try {
            const gameDoc = await GameModel.findByIdAndDelete(id);
            if (gameDoc == null)
                return false;
            return true;
        }
        catch (err) {
            throw (err);
        }
    }
};

module.exports = GameRepositoryDB;