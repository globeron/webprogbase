<<<<<<< HEAD
const userRepository = require('./repositories/userRepository');
const gameRepository = require('./repositories/gameRepository');
const Game = require('./models/game');
const readline = require('readline-sync');


const userStorage = new userRepository('./data/users.json');
//
const gameStorage = new gameRepository('./data/games.json');


function isDate(str)
{    
    const parms = str.split(/[\:\.\-\/]/);
    const yyyy = parseInt(parms[0],10);
    const mm   = parseInt(parms[1],10);
    const dd   = parseInt(parms[2],10);
    const h   = parseInt(parms[3],10);
    const m   = parseInt(parms[4],10);
    const s   = parseInt(parms[5],10);
    const ms   = parseInt(parms[6],10);
    const date = new Date(yyyy,mm-1,dd,h,m,s,ms);
    return mm == (date.getMonth()+1) && 
        dd == date.getDate() && 
        yyyy == date.getFullYear() &&
        h == date.getHours() &&
        m == date.getMinutes() &&
        s == date.getSeconds() &&
        ms == date.getMilliseconds();
}

while (true)
{
    while (true) 
    {
        const inputRaw = readline.question("Enter command:");
        const input = inputRaw.substr(0,inputRaw.lastIndexOf('/')+1);
        if (inputRaw === "get/games")
        {
            const games = gameStorage.getGames();
            for(game of games)
            {
                console.log(game.id+'|'+game.name +'|'+ game.score);
            }
            
        }
        else if (input === "get/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const game = gameStorage.getGameById(id);
            if(game!=null)
                console.log(game.id+'|'+game.name+'|'+game.studio+'|'+game.price+'|'+game.score+'|'+game.date);
            else
                console.error("Wrong id");
        }
        else if (input === "delete/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const game = gameStorage.delteGame(id);
            if(game)
                console.log('Successfully');
            else
                console.error("Wrong id");
        }
        else if (input === "update/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            ////
            let NewGame = new Game;
            NewGame.id = id;
            const name = readline.question("Enter name: ");
            NewGame.name=name;
            //
            const studio = readline.question("Enter studio: ");
            NewGame.studio=studio;
            //
            const price = readline.question("Enter price: ");
            if(isNaN(price))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.price=price;
            //
            const score = readline.question("Enter score: ");
            if(isNaN(score))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.score=score;
            //
            const date = readline.question("Enter date: ");
            if(!isDate(date))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.date=date;
            ////
            const game = gameStorage.updateGame(NewGame);
            if(game)
                console.log('Successfully');
            else
                console.error("Wrong id");
        }
        else if (inputRaw === "post/games")
        {
            let NewGame = new Game;
            const name = readline.question("Enter name: ");
            NewGame.name=name;
            //
            const studio = readline.question("Enter studio: ");
            NewGame.studio=studio;
            //
            const price = readline.question("Enter price: ");
            if(isNaN(price))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.price=price;
            //
            const score = readline.question("Enter score: ");
            if(isNaN(score))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.score=score;
            //
            const date = readline.question("Enter date: ");
            if(!isDate(date))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.date=date;
            //
            console.log('New id: '+gameStorage.addGame(NewGame));
        }
        else if(inputRaw === "get/users")
        {
            const users = userStorage.getUsers();
            for(user of users)
            {
                console.log(user.id+'|'+user.login);
            }
        }
        else if(input === "get/users/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const user = userStorage.getUserById(id);
            if(user!=null)
                console.log(user.id+'|'+user.login+'|'+user.fullname+'|'+user.role+'|'+user.registeredAt+'|'+user.avaUrl+'|'+user.isEnable);
            else
                console.error("Wrong id");
        }
        else
        {
            console.error("Unknown command");
        }
    }
=======
const userRepository = require('./repositories/userRepository');
const gameRepository = require('./repositories/gameRepository');
const Game = require('./models/game');
const readline = require('readline-sync');


const userStorage = new userRepository('./data/users.json');
//
const gameStorage = new gameRepository('./data/games.json');


function isDate(str)
{    
    const parms = str.split(/[\:\.\-\/]/);
    const yyyy = parseInt(parms[0],10);
    const mm   = parseInt(parms[1],10);
    const dd   = parseInt(parms[2],10);
    const h   = parseInt(parms[3],10);
    const m   = parseInt(parms[4],10);
    const s   = parseInt(parms[5],10);
    const ms   = parseInt(parms[6],10);
    const date = new Date(yyyy,mm-1,dd,h,m,s,ms);
    return mm == (date.getMonth()+1) && 
        dd == date.getDate() && 
        yyyy == date.getFullYear() &&
        h == date.getHours() &&
        m == date.getMinutes() &&
        s == date.getSeconds() &&
        ms == date.getMilliseconds();
}

while (true)
{
    while (true) 
    {
        const inputRaw = readline.question("Enter command:");
        const input = inputRaw.substr(0,inputRaw.lastIndexOf('/')+1);
        if (inputRaw === "get/games")
        {
            const games = gameStorage.getGames();
            for(game of games)
            {
                console.log(game.id+'|'+game.name +'|'+ game.score);
            }
            
        }
        else if (input === "get/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const game = gameStorage.getGameById(id);
            if(game!=null)
                console.log(game.id+'|'+game.name+'|'+game.studio+'|'+game.price+'|'+game.score+'|'+game.date);
            else
                console.error("Wrong id");
        }
        else if (input === "delete/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const game = gameStorage.delteGame(id);
            if(game)
                console.log('Successfully');
            else
                console.error("Wrong id");
        }
        else if (input === "update/games/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            ////
            let NewGame = new Game;
            NewGame.id = id;
            const name = readline.question("Enter name: ");
            NewGame.name=name;
            //
            const studio = readline.question("Enter studio: ");
            NewGame.studio=studio;
            //
            const price = readline.question("Enter price: ");
            if(isNaN(price))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.price=price;
            //
            const score = readline.question("Enter score: ");
            if(isNaN(score))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.score=score;
            //
            const date = readline.question("Enter date: ");
            if(!isDate(date))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.date=date;
            ////
            const game = gameStorage.updateGame(NewGame);
            if(game)
                console.log('Successfully');
            else
                console.error("Wrong id");
        }
        else if (inputRaw === "post/games")
        {
            let NewGame = new Game;
            const name = readline.question("Enter name: ");
            NewGame.name=name;
            //
            const studio = readline.question("Enter studio: ");
            NewGame.studio=studio;
            //
            const price = readline.question("Enter price: ");
            if(isNaN(price))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.price=price;
            //
            const score = readline.question("Enter score: ");
            if(isNaN(score))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.score=score;
            //
            const date = readline.question("Enter date: ");
            if(!isDate(date))
            {
                console.error('Wrong input');
                break;
            }
            NewGame.date=date;
            //
            console.log('New id: '+gameStorage.addGame(NewGame));
        }
        else if(inputRaw === "get/users")
        {
            const users = userStorage.getUsers();
            for(user of users)
            {
                console.log(user.id+'|'+user.login);
            }
        }
        else if(input === "get/users/")
        {
            const id = inputRaw.substr(inputRaw.lastIndexOf('/')+1);
            const user = userStorage.getUserById(id);
            if(user!=null)
                console.log(user.id+'|'+user.login+'|'+user.fullname+'|'+user.role+'|'+user.registeredAt+'|'+user.avaUrl+'|'+user.isEnable);
            else
                console.error("Wrong id");
        }
        else
        {
            console.error("Unknown command");
        }
    }
>>>>>>> e7509f2b0aff151da44e77d68e96fd2bc06187bd
}